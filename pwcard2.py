"""
Generates a pseudo-random card useful for producing passwords.

Most of the following is basically just a brain dump, or a dialog with myself, so it may be
somewhat inconsistent and babbling.

About the Card
==============

The card is a grid where each cell is a character from an alphabet of 72
possible characters. However, characters are not all chosen independently: they are
chosen in chunks of four characters each. Each chunk contains exactly one uppercase
letter, one lowercase letter, one digit, and one special symbol. More over, the two
letters always come first followed by the digit and symbol. The order of the letters (uppercase
first or lowercase first), and the order of the digit/symbol pair (digit first or symbol first)
is chosen pseudo-randomly for each chunk, and the specific character chosen for each
class is also chosen pseudo-randomly and independently.

This does reduce the entropy density of passwords (meaning they need to be somewhat longer for the same
level of security), but it makes the password much more general in the face of
increasingly demanding password policies. Specifically, this ensures that no password
has any "double" characters like "xx" or "44", and any password that is at least four characters
long (they better be!) has at least one character from each of the classes that password
policies typically demand.

The entropy of a given chunk is 1 + 1 + log2(24) + log2(24) + log2(8) + log2(16), slightly more
than 18.16 bits. If every single character was chosen independetly from the alphabet of 72 characters,
chunks would be significantly more dense, at about 24.67 bits per chunk, but it would produce a lot
more passwords that password policies would reject.

How to use the PWCard
=====================

The easiest approach is to simply pick two characters that represent your account,
and use them as coordinates to find the start of the password in the PWCard. For
instance, you might use "gm" for gmail: go to column 'G' and row 'M', find where they
meet, and start reading off however many characters you want from there.

How you read off is up to you. The simplest approach is to just read off in a straight
line left or right. Left to right is probably easier for most European language readers,
and if you get to the end of the row you can just roll over back to the beginning of the
row.

A more complex pattern will be more secure, but harder to use. For instance, you could
read in a spiral, or a zig-zag, or whatever else you want. A solution that is both more
secure and more easy to use than any of these is to create a random grille with a defined
starting point that can be overlaid onto the selected cell from the card. To make sure
the grille fits on the card, put the starting point in one corner of the grille and
flip the grille in whatever way needed to make it fit. Or, to be more consistent, flip
the grille depending on which quadrant of the card you're starting in. For instance,
if your starting in the upper-left quadrant, flip the grill so that the grille's
starting point is also in the upper-left. Make your grill a rectangle and always hold it
in "portrait" position (matching the pwcard) so that the long side is vertical,
and always read the grille from left to right, then top to bottom. Note that this means
you won't actually be starting at the selected cell necessarily, but that's totally fine.
It's good to have a consistent way to read so you're less likely to make mistakes.

When making a grille, every slot should be 5 characters wide, and they should be aligned
so that they all start in the same chunk position. E.g., if your first slot is lined up so
that it starts at the second character of its chunk, then all slots should start at the
second character of their chunk. This will prevent your password from having any
"double" characters because some password policies don't allow that.

You can use more-than two characters for an account, in case, for instance, you need
a password for both gmail and General Mills. Two do this, you can simply replace the first
two characters with the character that they key in the pward, and now continue with that
character and the next character in your key. For instance, if you want to use "GMI" as your
key, you could replace "GM" with the character they select from the card, say "X", and then
use "XI" to continue. We can write this as "(GM)I". You can continue this indefinitely, e.g.,
"(((GM)I)L)L". You could do more complicated collapsing, like "((GM)I)(LL)", but that can
become very tedious and error prone, plus you need to remember the way you did it for each
account.

Of course, you should change your password pretty regularly. You can easily do this by
simply including a number at the _beginning_ of your key, and increment it when you need to
change it. So for instance, I might start my gmail password keyed by "(1G)M", then
go to "(2G)M", and so on. It's important that you do the number first because if you do it at the
end, all it will do is shift your entire password by one column. Most of the letters will be the
same, making it pretty trivial to guess if the preceding password is known, and if both passwords
are known, it will start to reveal more information about your entire pwcard which could be useful
in determining passwords for other accounts.

Even better, put the number at the beginning _and end_ of your key, and collapse it in pairs, like
"(1G)(M1)", "(2G)(M2)", etc. this will ensure that your key isn't always in the same row or column.

You'll notice at the top of the card, there are two sets of headers. The first set has all the digits
in order (and repeating) plus a random permutation of the alphabet. The numbers in this first set
can be used for generating new incremental passwords as described above. They are implicitly 1 through
24, and if you need to change your password more times than that, you'll just have to add some more
digits to the key.

The permutation of the alphabet can be used for simple encoding of your generated passwords if you
need to come up with one that's only letters for instance. Alternatively, you can use this row for some
or all of the letter-key-lookup into the card when finding a password: it's harder to do (takes longer to
search for the letter you need), but is more secure because it's harder for an attacker to predict how
two passwords are related (e.g., one keyed by "GM" and one keyed by "GN" are not necessarily one-column
off anymore).

Security
========

An individual password created with the PWCard is effectively random and therefore it's security
is a power function of it's length. There are 294,912 possible chunks. A 12 character password (3 chunks)
has over 25 quadrillion possible values: this gives you more than 54 bits of entropy, which is pretty solid,
but you probably want to check some contemporary standards. 

The main issue around security is protection of the PWCard itself. If someone gets your password card,
you're pretty well out of luck. The whole point of the card is that you use obvious keys with a secret
only you have (the card itself) to encode your passwords. If they get that secret, it won't be hard for
them to guess that "GM", or maybe "(1G)(M1)", or whatever, is your gmail password.

But there are still some things we can do to make it harder. First things first: always keep a copy of
your pwcard (someplace safe, like a safe) so that when you realize you've lost a copy of it, you can
generate a new card from new secrets and go change all your passwords.

Using a random grille, as described above, will make your passwords more secure, even if someone gets
your card. Unless of course they get your grille, too, which is very likely since you probably keep
them together.

So the other thing you can do is encode your keys. An easy way to do this is to pick two _random_
characters from the alphabet and keep them secret. Put one letter at the beginning of every key, and the
other at the end. Say we choose "X" and "Y" as our random letters. Now our key for gmail might be
"((XG)M)Y". Now your attacker can't just go to "GM" to find your password. Assuming they know the key, at its
core, is "GM": they're know dealing with "((?G)M)?". They know the first question mark will give them
a character somewhere in the
"G" column, but there's 24 rows in that column, and hopefully a good number of them are unique. For each
of those possible values, they've reduced the keyspace to "(?M)?": they know the first pair will be
somewhere in the M column, but again, there are 24 rows in this column.

But notice something happened here: they don't need to do 24*24 trials: they can just skip straight to
column M: whatever "(?G)" reduced to, the next pair is going to be in column M.

In the end, they will be left with "??", which is theoretically anywhere on the grid. However, in practice,
column M will not generally contain a character for of the 24 possible rows, and so the first "?" in the
final pair might be able to be reduced to some number less than 24. Likewise, column G may not contain
characters for all 24 possible rows which means "?G" can also be reduced, and therefore "(?G)M" could
potentially be reduced even further.

This is interesting because adding more "knowable" characters to the core-key ("GM") can actually reduce
the attacker's key space and make it easier for them to find your password. In an extreme case,
it could be that the intersections of all these possible subsets leaves the attacker with a single
row to search.

Meanwhile, adding more characters to the key actually makes it significantly more tedious and error prone
for us to use our own password card. The best keys will just be two randomly chosen characters. However,
this somewhat defeats the purpose of the keys: they're supposed to be trivial for you to remember.

Granted, it's a lot easier to remember a pair of random characters per service than an entire random
password, but is it worth it?

The grid is 24x24, there are only 576 possible starting locations for passwords. In the world of computers,
that's a trifle of a number. If the attacker has or can guess your grille, it will not take long at all
for them to find all possible passwords from the pwcard.

So if we reduce the number of starting locations from 576 to, say 200, it's not really that big of a deal
because the security is already pretty well compromised if this happens. It's best to keep a tight grip
on your password card and grille, and then just make it easy on yourself by using simple easy to remember
keys.

But that doesn't mean you need to just hand over your password. I would still use a pair of random letters
at the beginning and end of your key. But then, for the passwords you use most often, keep it to just
a single letter if you can, and a number so you can change it periodically.

For instance, if I choose "X" and "Y" as my secret characters, my google password is keyed by "((X1)G)Y"
to start, then "((X2)G)Y", etc. To an attacker, this will look like "((?1)G)?" at worst, or "(?G)?" if they
dont' know which number I'm on (though they can probably guess that it's a relatively small number).

So the moral of the story is: don't loose your password card, and especially don't loose your password card
and your grill.

A less severe case is that someone is able to get a couple of your passwords from a breach at one of the
services you subscribe to, because this happens kind of a lot. If the service has decent security, then
they won't get your actual password, but a hash of it. Well, I can just about guarantee that your password
isn't showing up in any rainbow tables, so they're basically reduced to brute forcing your password,
albeit with a pretty quick way to check their guesses. A twelve character (3 chunk) password will probably
buy you some pretty good time, especially if the password is salted and hashed appropriately, but if the attacker
really wants your password, they'll get it eventually.

Hopefully, someone at the service noticed soon that passwords were disclosed and they take appropriate
reset actions or whatever. The bigger question is: can they use this to get all your other passwords?

Most people use the same password for lots of accounts, or at least very similar passwords. The whole point
of the pwcard is to make it easy for you to use different passwords for each account, so you're in a much
better starting point than most people.

But a lot of your passwords could have some overlap. For instance, trivially, "GM" and "GE" could share a lot
of the same characters, depending on how you're reading off the passwords from the card. Using a random grille
should help considerably with this: you've always got at least 13 rows you can read from, so your grille shouldn't
use more than one chunk per row, and then passwords should all have at least one out of four characters distinct
from one another (other than by chance). That's not especially good, and is a good argument for using longer
passwords: a three chunk password may reasonably be different in just three characters.

This kind of attack could make it a bit easier for an attacker to guess a few other passwords, ones that are
closely related to the recovered one. A more general concern is if the attacker can start piecing together your
actual pwcard in order to recover (in full or in part) even more of your passwords. For instance, if they recover
your gmail password, they might guess that those characters fill in the "G" row, starting in column "M". This will
make it easier to guess your "GE" password, and if they can confirm that, they can start filling in more of your PWCard,
and so on.

Using the two secret characters (surrounding your key, as described above) will help for both types of attack (trying
to recover closely related passwords, and the more general attack of trying to fill in the pwcard), because it will be harder
for an attacker to guess how to passwords are related and where they are placed. For instance, "GE" and "GM" have a very specific
relationship: the password keyed by "GM" is simply translated right by 7 characters relative to "GE". This much is independent of
how you read your passwords, assuming you read them in a consistent way (which is recommended even though its a bit less secure).

Passwords keyed by "(?G)E" and "(?G)M" have the same relationship, but it will at least be harder for an attacker to figure out
where on the PWCard they go (though they still know that they are both start on the same row). However, "((?G)E)?" and "((?G)M)?"
have a much less well defined relationship, because without having the PWCard, the attacker doesn't know how "(?G)E" or "(?G)M"
reduce.

However, they do still know that they're in the same column because of that last "?". In fact, all of your passwords are going
to be in the same column for that reason. So a better solution might be "((X1)G)(MY)". In otherwords, always pair your final
secret character with the last character of your core key, which means we're back to needing two character. But to an 
attacker, this looks like "((?1)G)(M?)" (at best), and they don't know (exactly) how "(?1)G" _or_ "(M?)" reduces, so they
don't know how your two passwords relate to each other, or where they fit on the PWCard.

That said, if they find two passwords that have a lot of characters in common, that might give them significant information
about how they relate to each other. If they get enough such information, they might be able to start making connections
that allow them to fill in even more information, and so on. theoretically, they could get enough indirect information
from disclosed passwords to determine your entire PWCard, the secret key characters you use, and even your grille.

It would take quite a bit of work to get this, and probably a lot of time, so just plan to change your PWCard every
five years or so.


"""


from Crypto.Hash import HMAC, SHA256
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Cipher import AES
from Crypto.Util import Counter

import base64
import sys
import math

class PRGen(object):
    """
    A psuedo random generator, built from a cipher repeatedly encrypting a value of 0.
    So you should use someething like CTR mode.
    """

    def __init__(self, cipher):
        self.cipher = cipher
        self.buffer = []

    def bufferOctet(self):
        """
        Adds another 8 bits to the `buffer`.
        """
        o = ord(self.cipher.encrypt(chr(0)))
        for i in xrange(8):
            b = (o & 0x80) >> 7
            o = (o & 0x7F) << 1
            self.buffer.append(b)

    def ensureBuffer(self, bitcount):
        while len(self.buffer) < bitcount:
            self.bufferOctet()
        
    def nextBit(self):
        """
        get a pseudo-random bit.
        """
        self.ensureBuffer(1)
        return self.buffer.pop(0)

    def next(self, limit):
        """
        Get the random pseudo-random integer which is no greater than the specified
        limit.
        """
        if limit == 0:
            return 0
        if limit == 1:
            return self.nextBit()
        bits = int(math.ceil(math.log(limit, 2)))
        while True:
                self.ensureBuffer(bits)

                pool = 0
                for i in xrange(bits):
                    b = self.buffer.pop(0)
                    pool = (pool << 1) | b

                if pool <= limit:
                    return pool

    def permute(self, seq):
        """
        Uses Knuth shuffle to create pseudo-random permutation of the given iterable.
        """
        p = [c for c in seq]
        n = len(seq)
        for i in xrange(n):
            j = i + self.next(n-1-i)
            swap = p[i]
            p[i] = p[j]
            p[j] = swap
        return p

class Pwcard2(object):

    __uppercase = 'ABCDEFGHIJKMNPQRSTUVWXYZ'
    __lowercase = 'abcdefghijkmnpqrstuvwxyz'
    __digits = '23456789'
    __symbols = '!@#$%^&*()[].?-='

    def __init__(self, random_key, password, cost=1000):
        """
        Combine something you have (``random_key``) and something you know (``password``).
        The ``cost`` parameter specifies how much computational cost it takes to calculate
        the pwcard. Higher will take longer but is more secure against brute force attacks.
        """
        self.cost = cost
        self.random_key = random_key
        self.password = password
        self._generate()

    def pbkdf(self, password, salt, length):
        """
        Convenience function for deriving a key from a password.
        Uses the PBKDF2 method with SHA256. The number of iterations is determined by the `cost`
        attribute.

        This method is idempotents.

        :param str password: The password from which to derive the key.
        :param str salt: The random salt to use for derivation.
        :param int length: The number of octets of key to generate.
        """
        prf = lambda p,s: HMAC.new(p,s,SHA256).digest()
        return PBKDF2(password, salt, dkLen=length, count=self.cost, prf=prf)

    def deriveKeys(self):
        """
        Generate a pair of 16-octet keys from the configured `password` and `random_key`,
        using the `pbkdf` method.

        This method is idempotent.
        """
        derivedKey = self.pbkdf(self.password, self.random_key, 32)
        return derivedKey[:16], derivedKey[16:]


    def _newRng(self, rng):
        """
        Derive a new RNG from the given RNG.

        This is **not** idempotent, because it pulls bits from the given RNG.
        """
        #Create a pseudo-random 16 octet "password" from the rng.
        password = ''.join(chr(rng.next(256)) for i in xrange(16))

        #And a pseudo-random 16 octet salt from the RNG.
        salt = ''.join(chr(rng.next(256)) for i in xrange(16))

        #Now derive a new key from the generated password and salt.
        key = self.pbkdf(password, salt, 16)

        #Create a cipher in CTR mode from that key.
        cipher = AES.new(key, AES.MODE_CTR, counter=Counter.new(128))

        #And create a new RNG from that cipher.
        return PRGen(cipher)


    def _nextChunk(self, rng):
        """
        Generate the next chunk of four pseudo-random characters to build out the
        password card, using the given `PRGen`.

        This is **not** idempotent, because it pulls bits from the given RNG.
        """
        rng = self._newRng(rng)

        return (rng.nextBit(), rng.nextBit(), rng.next(len(self.__lowercase)-1), rng.next(len(self.__uppercase)-1), rng.next(len(self.__digits)-1), rng.next(len(self.__symbols)-1))


    def _chunkToChars(self, chunk):

        #We always do letters first, to prevent duplicates (because some password policies seem to think that
        # "abcd" is a better password than "19jxx6"). But do we want to do upper case or lower case first?
        low_first = chunk[0]
        #For the next two chars in the chunk, do we want digit first, or symbol?
        dig_first = chunk[1]

        #Choose the characters to use for each for the four classes of characters in the chunk.
        lower = self.__lowercase[chunk[2]]
        upper = self.__uppercase[chunk[3]]
        digit = self.__digits[chunk[4]]
        symbol = self.__symbols[chunk[5]]

        #Assemble the chunk
        if low_first:
            chunk = lower + upper
        else:
            chunk = upper + lower

        if dig_first:
            chunk += digit + symbol
        else:
            chunk += symbol + digit

        return chunk
        
        

    def _generate(self):
        """
        Generate the contents of this password card, and store internally.
        
        This method is idempotent, but it can take a while if you have a high `cost`. It's called
        from `__init__` anyway, so you probably don't need to call it.
        """
        #Create some pseudo-random keys from our configured secrets, use them to create some
        # cipher-driven pseudo-random generators.
        k1, k2 = self.deriveKeys()
        cipher1 = AES.new(k1, AES.MODE_CTR, counter=Counter.new(128))
        cipher2 = AES.new(k2, AES.MODE_CTR, counter=Counter.new(128))
        r1 = PRGen(cipher1)
        r2 = PRGen(cipher2)

        #Make sure our alphabets are configured correctly.
        if len(self.__uppercase) != 24 or len(self.__lowercase) != 24 or (len(self.__digits) + len(self.__symbols)) != 24:
            raise ValueError('Alphabets are wrong.')

        #Generate a pseudo-random permutation of the alphabet, which might be useful for something, but I can't remember
        # what at the moment.
        self.permutation = ''.join(r2.permute(self.__uppercase))

        #Rows is an array of arrays of chunk tuples. The outer most array is the array of row arrays.
        # Each row is an array whose elements are tuples representing a chunk.
        self.rows = [[self._nextChunk(r1) for i in xrange(6)] for j in xrange(24)]

    def printText(self):
        """
        Prints out a textual representation of the card.
        """

        print '   | ' + ' '.join(''.join(str((d+1)%10) for d in xrange(i, i+4)) for i in xrange(0, len(self.permutation), 4))
        print '   | ' + ' '.join(self.permutation[i:i+4] for i in xrange(0, len(self.permutation), 4))
        print '---+-' + ('-'*(5*6-1))
        headers = [self.__digits + self.__symbols, self.__uppercase]
        for cheader in headers:
            print '   | ' + ' '.join(cheader[i:4+i] for i in xrange(0, len(cheader), 4))
        print '---+-' + ('-'*(5*6-1))

        for rnum, row in enumerate(self.rows):
            print headers[0][rnum] + headers[1][rnum] + ' | ' + ' '.join(self._chunkToChars(c) for c in row)



random_key = base64.b64decode(r'0SGSGxUfdSDBR9tzZTeGkJqZP/XHpEWPFNub6mvoZaJqa4xMRRxkSaKUV+GxSlisdAquGHuKQqzk9lB7W3feZA==')
password = 'this is my secret password.'

def main():
    #FIXME: Change cost.
    pwcard = Pwcard2(password=password, random_key=random_key, cost=10)
    pwcard.printText()

if __name__ == '__main__':
    main()

