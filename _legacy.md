# General

26 lowercase letters
26 uppercase letters
8 digits, excluding 0 and 1
5 special symbol

Generated in blocks of 4 chars. The first two are letters, one upper and one lower.
The second two are one symbol and one digit. This ensures every password has one of
each class of characters, and no matched-pairs of chars.

2 options - Upper case first or second
2 options - Digit first or second
26*26 options - Two letters
8*5 - One digit and one symbol
-----
108160 ~ 16.7 bits per 4-char-group (4.18 bpc)

# Pronounceable

## Prefixes

* 19 consonants (less c and q)
* (22) qu, bl, br, kl, kr, dr, fl, fr, gl, gr, pl, pr, sk, sl, sm, sn, sp, squ, st, str, sw, tr
-----
41 total

## Suffixes

* 19 consonants (less c and q)
* (21) kt, ft, lb, ld, lf, lg, lk, lm, ln, lp, ls, lt, mp, nd, ng, nk, nt, pt, sk, sp, st
-----
40 total

